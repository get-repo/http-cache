<p align="center">
    <img src="https://gitlab.com/uploads/-/system/project/avatar/40226015/cache_logo.png" height=100 />
</p>

<h1 align=center>HTTP Cache bundle</h1>

Cache Invalidation process that allows you to keep your website optimized, save server resources, and reduce the loading time.

<br/>

## **Installation**

#### Installation with composer
```sh
composer config repositories.get-repo/http-cache git https://gitlab.com/get-repo/http-cache.git
composer require get-repo/http-cache
```
## Enable the bundle

In *config/bundles.php*
```php
<?php
return [
    ...
    GetRepo\HttpCache\GetRepoHttpCache::class => ['prod' => true],
];
```

## **Configuration reference**

Default possible expression language values :
 - cache *(AbstractAdapter)*
 - route *(route name from `_route` request attribute)*
 - request *(Request)*
 - user *(UserInterface or NULL)*
 - user_roles  *(array)*

In *config/packages/getrepo_http_cache.yaml*
```yaml
getrepo_http_cache:
    enabled: true # fully enable or disable the http cache bundle (bool or expression language)
    cache_factory: 'GetRepo\HttpCache\Cache\DefaultCacheFactory' # service name
    options:
        throw_exception: [dev, test] # throw exception for these envs
        orm:
            events: # list of doctrine events to listen to
                - preRemove
                - postRemove
                - prePersist
                - postPersist
                - preUpdate
                - postUpdate
            clear: [App\Entity\Settings] # cache full clear when these entities changes
    definitions:
        # Prototype
        route_name:
            enabled: true # enable or disable this http cache definition (bool or expression language)
            cache:
                condition: 'request.getMethod() == "GET"' # optional expression language condition with values (route, request)
                key: '"ROUTE_KEY"' #  expression language cache key
            invalidations: 
                routes: # invalidation by route
                    # Prototype
                    route_name:
                        condition: null
                        key: route
                orm: # invalidation by entity
                    # Prototype
                    App\Entity\Book:
                        condition: 'entity.id > 10' # optional expression language condition with values (cache, route, entity, entity_class)
                        key: 'entity_key_cache' # Required
                customized: # invalidation custom
                    specific key: "my_specific_cache_key"
                    force clear cache:
                        condition: 'request.query.get("blabla")' # optional expression language condition with values (cache, route, request)
                        key: 'cache.clear()'
```

## **Configuration examples**

In *config/packages/getrepo_http_cache.yaml*
```yaml
getrepo:
    definitions:
        # always cached
        route_name_1:
            cache: '"KEY_NAME_ROUTE_1"'

        # always cached with different key by id
        route_name_2:
            cache:
                key: 'route ~ "_" ~ request.attributes.get("_route_params")["id"]'

        # always cached with condition, only POST
        route_name_3:
            cache:
                key: '"KEY_NAME_ROUTE_3"'
                condition: 'request.getMethod() == "POST"'

        # always cached with invalidation by route name
        route_name_4:
            cache: '"KEY_NAME_ROUTE_4"'
            invalidations:
                # invalidation by route name
                routes:
                    # When I visit route_name_1, I invalidate cache for route_name_1
                    route_name_1:
                        key: '"KEY_NAME_ROUTE_1"'

        # always cached with invalidation by entity
        route_name_5:
            cache: '"KEY_NAME_ROUTE_5"'
            invalidations:
                # invalidation by route name
                orm:
                    # When I persist a book entity, I invalidate cache for route_name_4
                    App\Entity\Book:
                        key: '"KEY_NAME_ROUTE_4"'

        # always cached with custom invalidation
        route_name_6:
            cache: '"KEY_NAME_ROUTE_6"'
            invalidations:
                # custom invalidation
                customized:
                    # When I call a service and a string is returned as key
                    App\Entity\Book:
                        condition: 'container.get("my_service").getCache(request)'
```




## **Handlers**

### CacheHandler
Handle the cache
`GetRepo\HttpCache\Handler\CacheHandler`

### InvalidationHandler
Handle the invalidation
`GetRepo\HttpCache\Handler\InvalidationHandler`
