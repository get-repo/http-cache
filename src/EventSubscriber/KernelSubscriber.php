<?php

namespace GetRepo\HttpCache\EventSubscriber;

use GetRepo\HttpCache\Handler\CacheHandler;
use GetRepo\HttpCache\Handler\InvalidationHandler;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

#[AsEventListener(event: KernelEvents::CONTROLLER, method: 'onController')]
#[AsEventListener(event: KernelEvents::TERMINATE, method: 'onTerminate')]
class KernelSubscriber
{
    public function __construct(
        private readonly CacheHandler $cacheHandler,
        private readonly InvalidationHandler $invalidationHandler,
        #[Autowire(param: 'getrepo_httpcache.config')]
        private readonly array $config,
        #[Autowire(param: 'kernel.environment')]
        private readonly string $env,
    ) {
    }

    public function onController(ControllerEvent $event): void
    {
        $request = $event->getRequest();

        try {
            // get cache if exists
            if ($content = $this->cacheHandler->getCache($request)) {
                $event->stopPropagation();
                $event->setController(fn () => new Response($content));
            }
        } catch (\Exception $e) {
            if (in_array($this->env, $this->config['options']['throw_exception'])) {
                throw $e;
            }
        }
    }

    public function onTerminate(TerminateEvent $event): void
    {
        $request = $event->getRequest();
        $response = $event->getResponse();

        try {
            $this->cacheHandler->setCache($request, $response);
            $this->invalidationHandler->invalidateRoutes($request);
            $this->invalidationHandler->invalidateCustomized($request);
        } catch (\Exception $e) {
            if (in_array($this->env, $this->config['options']['throw_exception'])) {
                throw $e;
            }
        }
    }
}
