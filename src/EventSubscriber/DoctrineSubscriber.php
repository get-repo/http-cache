<?php

namespace GetRepo\HttpCache\EventSubscriber;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use GetRepo\HttpCache\Handler\InvalidationHandler;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

#[AsDoctrineListener(event: Events::prePersist)]
#[AsDoctrineListener(event: Events::postPersist)]
#[AsDoctrineListener(event: Events::preUpdate)]
#[AsDoctrineListener(event: Events::preUpdate)]
#[AsDoctrineListener(event: Events::preRemove)]
#[AsDoctrineListener(event: Events::postRemove)]
class DoctrineSubscriber
{
    public function __construct(
        private readonly InvalidationHandler $invalidationHandler,
        #[Autowire(param: 'getrepo_httpcache.config')]
        private readonly array $config
    ) {
    }

    public function __call(string $method, array $args): void
    {
        if (in_array($method, $this->config['options']['orm']['events'] ?? []) && $args) {
            $this->invalidate($args[0]);
        }
    }

    private function invalidate(LifecycleEventArgs $event): void
    {
        $this->invalidationHandler->invalidateEntity($event->getObject());
    }
}
