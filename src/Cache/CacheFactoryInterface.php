<?php

namespace GetRepo\HttpCache\Cache;

use Symfony\Component\Cache\Adapter\AbstractAdapter;

interface CacheFactoryInterface
{
    public function buildCache(): AbstractAdapter;
}
