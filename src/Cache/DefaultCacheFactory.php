<?php

namespace GetRepo\HttpCache\Cache;

use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class DefaultCacheFactory implements CacheFactoryInterface
{
    public function __construct(
        #[Autowire(service: 'getrepo_httpcache.http_cache')]
        private AbstractAdapter $cache,
    ) {
    }

    public function buildCache(): AbstractAdapter
    {
        return $this->cache;
    }
}
