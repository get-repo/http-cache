<?php

namespace GetRepo\HttpCache\Handler;

use GetRepo\HttpCache\Cache\CacheFactoryInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\HttpFoundation\Request;

abstract class AbstractHandler
{
    public function __construct(
        protected CacheFactoryInterface $cacheFactory,
        protected Security $security,
        #[Autowire(service: 'getrepo_httpcache.expression_language')]
        protected ExpressionLanguage $expressionLanguage,
        #[Autowire(param: 'getrepo_httpcache.config')]
        protected array $config
    ) {
    }

    protected function buildCache(): AbstractAdapter
    {
        return $this->cacheFactory->buildCache();
    }

    protected function getExpressionLanguageValues(Request $request, array $extras = []): array
    {
        $user = $this->security->getUser();

        return array_merge(
            [
                'cache' => $this->buildCache(),
                'route' => $request->attributes->get('_route'),
                'request' => $request,
                'user' => $user,
                'user_roles' => $user ? implode('-', $user->getRoles()) : 'anonymous',
            ],
            $extras
        );
    }
}
