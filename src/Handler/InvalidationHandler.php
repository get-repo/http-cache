<?php

namespace GetRepo\HttpCache\Handler;

use Doctrine\Common\Util\ClassUtils;
use Symfony\Component\HttpFoundation\Request;

class InvalidationHandler extends AbstractHandler
{
    public function invalidateRoutes(Request $request): void
    {
        if ($this->config['enabled'] && ($routeName = $request->attributes->get('_route'))) {
            $cache = $this->buildCache();
            foreach ($this->config['definitions'] as $conf) {
                $elValues = $this->getExpressionLanguageValues($request);
                if (isset($conf['invalidations']['routes'][$routeName])) {
                    $invalidation = $conf['invalidations']['routes'][$routeName];
                    if (
                        !$invalidation['condition']
                        || $this->expressionLanguage->evaluate($invalidation['condition'], $elValues)
                    ) {
                        $key = (string) $this->expressionLanguage->evaluate(
                            $invalidation['key'],
                            $elValues
                        );
                        if ($key && $cache->hasItem($key)) {
                            $cache->deleteItem($key);
                        }
                    }
                }
            }
        }
    }

    public function invalidateEntity(object $entity): void
    {
        if ($this->config['enabled'] && is_object($entity)) {
            $entityClass = ClassUtils::getRealClass($entity::class);
            $cache = $this->buildCache();
            if (in_array($entityClass, $this->config['options']['orm']['clear'] ?? [])) {
                $cache->clear();
                return;
            }
            foreach ($this->config['definitions'] as $conf) {
                $elValues = $this->getExpressionLanguageValues(
                    new Request(),
                    [
                        'entity' => $entity,
                        'entity_class' => $entityClass,
                    ],
                );
                if (isset($conf['invalidations']['orm'][$entityClass])) {
                    $invalidation = $conf['invalidations']['orm'][$entityClass];
                    if (
                        !$invalidation['condition']
                        || $this->expressionLanguage->evaluate($invalidation['condition'], $elValues)
                    ) {
                        $key = (string) $this->expressionLanguage->evaluate(
                            $invalidation['key'],
                            $elValues
                        );
                        if ($key && $cache->hasItem($key)) {
                            $cache->deleteItem($key);
                        }
                    }
                }
            }
        }
    }

    public function invalidateCustomized(Request $request): void
    {
        if ($invalidations = $this->getInvalidationConfig($request, 'customized')) {
            $cache = $this->buildCache();
            $elValues = $this->getExpressionLanguageValues($request);
            foreach ($invalidations as $invalidation) {
                if (
                    !$invalidation['condition']
                    || $this->expressionLanguage->evaluate($invalidation['condition'], $elValues)
                ) {
                    $key = (string) $this->expressionLanguage->evaluate(
                        $invalidation['key'],
                        $elValues
                    );
                    if ($key && $cache->hasItem($key)) {
                        $cache->deleteItem($key);
                    }
                }
            }
        }
    }

    private function getInvalidationConfig(Request $request, string $type): ?array
    {
        if (
            $this->config['enabled']
            && ($routeName = $request->attributes->get('_route'))
            && isset($this->config['definitions'][$routeName]['invalidations'][$type])
        ) {
            return $this->config['definitions'][$routeName]['invalidations'][$type];
        }

        return null;
    }
}
