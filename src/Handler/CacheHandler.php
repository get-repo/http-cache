<?php

namespace GetRepo\HttpCache\Handler;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CacheHandler extends AbstractHandler
{
    final public const CACHE_ATTRIBUTE_NAME = '_httpcached';

    private ?string $key = null;

    public function getCache(Request $request): ?string
    {
        if ($this->isEnabled($this->config['enabled'], $request)) {
            if ($this->key = $this->getCacheKey($request)) {
                $cache = $this->buildCache();
                if ($cache->hasItem($this->key)) {
                    $cached = $cache->getItem($this->key)->get();
                    $request->attributes->set(self::CACHE_ATTRIBUTE_NAME, true);

                    return $cached['content'] ?? null;
                }
            }
        }

        return null;
    }

    public function setCache(Request $request, Response $response): void
    {
        // expects method getCache() has been called before
        if (
            $this->isEnabled($this->config['enabled'], $request)
            && !$request->attributes->get(self::CACHE_ATTRIBUTE_NAME)
            && $this->key
        ) {
            $cache = $this->buildCache();
            $cacheItem = $cache->getItem($this->key);
            $cacheItem->set([
                'key' => $this->key,
                'content' => $response->getContent(),
                'request' => $this->requestToArray($request),
            ]);
            $cache->save($cacheItem);
        }
        $this->key = null;
    }

    private function getCacheKey(Request $request): ?string
    {
        $routeName = $request->attributes->get('_route');
        if (isset($this->config['definitions'][$routeName])) {
            $conf = $this->config['definitions'][$routeName];
            $elValues = $this->getExpressionLanguageValues($request);
            if (
                $this->isEnabled($conf['enabled'], $request) // if route cache is enabled
                && $conf['cache']['key'] // if route cache has key
                && ( // condition
                    !$conf['cache']['condition']
                    || $this->expressionLanguage->evaluate($conf['cache']['condition'], $elValues)
                )
            ) {
                return (string) $this->expressionLanguage->evaluate($conf['cache']['key'], $elValues);
            }
        }

        return null;
    }

    private function requestToArray(Request $request): array
    {
        $array = [];
        foreach ((array) $request as $key => $value) {
            $key = trim($key, chr(0) . chr(42));
            /** @var \Symfony\Component\HttpFoundation\ParameterBag $value */
            if ($value instanceof \IteratorAggregate) {
                switch ($key) {
                    case 'attributes':
                        $value = [
                            'controller' => $value->get('_controller'),
                            'route' => $value->get('_route'),
                            'route_params' => $value->get('_route_params'),
                        ];
                        break;
                    case 'files':
                    case 'server':
                    case 'session':
                    case 'cookies':
                        continue 2;
                        break; // @phpstan-ignore-line
                    default:
                        $value = $value->all();
                        break;
                }
            }
            $array[$key] = $value;
        }

        return $array;
    }

    private function isEnabled(bool|string $value, Request $request): bool
    {
        if (is_bool($value)) {
            return $value;
        }

        return (bool) $this->expressionLanguage->evaluate($value, $this->getExpressionLanguageValues($request));
    }
}
