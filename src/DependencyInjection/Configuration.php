<?php

namespace GetRepo\HttpCache\DependencyInjection;

use Doctrine\ORM\Events;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(GetRepoHttpCache::ALIAS);
        /** @var \Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode // @phpstan-ignore-line
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('enabled')
                    ->defaultTrue()
                ->end()
                ->scalarNode('cache_factory')
                    ->defaultNull()
                ->end()
                ->arrayNode('options')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('throw_exception')
                            ->treatNullLike([])
                            ->defaultValue(['dev', 'test'])
                            ->scalarPrototype()->end()
                        ->end()
                        ->arrayNode('orm')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->arrayNode('events')
                                    ->defaultValue($this->getORMEvents())
                                    ->scalarPrototype()
                                        ->validate()
                                            ->ifNotInArray($this->getORMEvents())
                                            ->thenInvalid('Invalid event %s')
                                        ->end()
                                    ->end()
                                ->end()
                                ->arrayNode('clear')
                                    ->scalarPrototype()
                                        ->isRequired()
                                        ->cannotBeEmpty()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('definitions')
                    ->useAttributeAsKey('route')
                    ->arrayPrototype()
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('enabled')
                                ->defaultTrue()
                            ->end()
                            ->arrayNode('cache')
                                ->addDefaultsIfNotSet()
                                ->treatNullLike(['key' => null])
                                ->beforeNormalization()
                                    ->ifString()
                                    ->then(function ($key) {
                                        return ['key' => $key];
                                    })
                                ->end()
                                ->children()
                                    ->scalarNode('condition')
                                        ->defaultNull()
                                    ->end()
                                    ->scalarNode('key')
                                        // default route name as key
                                        ->defaultValue('route')
                                    ->end()
                                ->end()
                            ->end()
                            ->arrayNode('invalidations')
                                ->children()
                                    ->arrayNode('routes')
                                        ->useAttributeAsKey('route')
                                        ->arrayPrototype()
                                            ->addDefaultsIfNotSet()
                                            ->treatNullLike(['key' => null])
                                            ->beforeNormalization()
                                                ->ifString()
                                                ->then(function ($key) {
                                                    return ['key' => $key];
                                                })
                                            ->end()
                                            ->children()
                                                ->scalarNode('condition')
                                                    ->defaultNull()
                                                ->end()
                                                ->scalarNode('key')
                                                    // default route name as key
                                                    ->defaultValue('route')
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                    ->arrayNode('orm')
                                        ->useAttributeAsKey('entity')
                                        ->arrayPrototype()
                                            ->addDefaultsIfNotSet()
                                            ->treatNullLike(['key' => null])
                                            ->beforeNormalization()
                                                ->ifString()
                                                ->then(function ($key) {
                                                    return ['key' => $key];
                                                })
                                            ->end()
                                            ->children()
                                                ->scalarNode('condition')
                                                    ->defaultNull()
                                                ->end()
                                                ->scalarNode('key')
                                                    ->isRequired()
                                                    ->cannotBeEmpty()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                    ->arrayNode('customized')
                                        ->arrayPrototype()
                                            ->addDefaultsIfNotSet()
                                            ->treatNullLike(['key' => null])
                                            ->beforeNormalization()
                                                ->ifString()
                                                ->then(function ($key) {
                                                    return ['key' => $key];
                                                })
                                            ->end()
                                            ->children()
                                                ->scalarNode('condition')
                                                    ->defaultNull()
                                                ->end()
                                                ->scalarNode('key')
                                                    ->isRequired()
                                                    ->cannotBeEmpty()
                                                ->end()
                                            ->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

    private function getORMEvents(): array
    {
        $reflectionClass = new \ReflectionClass(Events::class);
        $events = $reflectionClass->getConstants();

        // remove non-lifecycle and no updates events.
        unset(
            $events[Events::loadClassMetadata],
            $events[Events::onClassMetadataNotFound],
            $events[Events::preFlush],
            $events[Events::onFlush],
            $events[Events::postFlush],
            $events[Events::onClear],
            $events[Events::postLoad],
        );

        return array_values($events);
    }
}
