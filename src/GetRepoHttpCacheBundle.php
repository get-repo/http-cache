<?php

namespace GetRepo\HttpCache;

use GetRepo\HttpCache\DependencyInjection\GetRepoHttpCache;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class GetRepoHttpCacheBundle extends Bundle
{
    public function getContainerExtension(): ?ExtensionInterface
    {
        if (null === $this->extension) {
            $this->extension = $this->createContainerExtension();
        }

        return $this->extension;
    }

    protected function getContainerExtensionClass(): string
    {
        return GetRepoHttpCache::class;
    }
}
