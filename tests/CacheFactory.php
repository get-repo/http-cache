<?php

namespace Test;

use GetRepo\HttpCache\Cache\CacheFactoryInterface;
use Symfony\Component\Cache\Adapter\AbstractAdapter;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class CacheFactory implements CacheFactoryInterface
{
    /** @return FilesystemAdapter */
    public function buildCache(): AbstractAdapter
    {
        return new FilesystemAdapter('test', 60, dirname(__DIR__) . '/var');
    }
}
