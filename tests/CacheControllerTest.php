<?php

namespace Test;

use GetRepo\SqliteDoctrineTest\SqliteTestCase;
use Test\Entity\Entity;

class CacheControllerTest extends SqliteTestCase
{
    final public const NB_CHECKS = 3;

    public function testNotCached(): void
    {
        $crawler = self::getClient()->request('GET', '/not-cached');
        $content = $crawler->html();

        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/not-cached');
            $this->assertNotEquals($content, $crawler->html());
        }
    }

    public function testDisabledGlobally(): void
    {
        $crawler = self::getClient()->request('GET', '/always-cached?FORCE_DISABLED');
        $cached = $crawler->html();

        // response should be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/always-cached?FORCE_DISABLED');
            $this->assertNotEquals($cached, $crawler->html());
        }
    }

    public function testDisabledByDefinition(): void
    {
        $crawler = self::getClient()->request('GET', '/always-cached?FORCE_DISABLED_ALWAYS_CACHED');
        $cached = $crawler->html();

        // response should be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/always-cached?FORCE_DISABLED_ALWAYS_CACHED');
            $this->assertNotEquals($cached, $crawler->html());
        }
    }

    public function testDisabledRoute(): void
    {
        $crawler = self::getClient()->request('GET', '/disabled');
        $content = $crawler->html();

        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/disabled');
            $this->assertNotEquals($content, $crawler->html());
        }
    }

    public function testAlwaysCachedRoute(): void
    {
        $crawler = self::getClient()->request('GET', '/always-cached');
        $cached = $crawler->html();

        // response should be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/always-cached');
            $this->assertEquals($cached, $crawler->html());
        }
    }

    public function testConditionRoute(): void
    {
        $crawler = self::getClient()->request('GET', '/condition/aaaa');
        $content = $crawler->html();
        // should not be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/condition/aaaa');
            $this->assertNotEquals($content, $crawler->html());
        }

        $crawler = self::getClient()->request('GET', '/condition/CACHED');
        $cached = $crawler->html();

        // should be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/condition/CACHED');
            $this->assertEquals($cached, $crawler->html());
        }
    }

    public function testConditionOnRequest(): void
    {
        $crawler = self::getClient()->request('GET', '/cached-if-get-param');
        $content = $crawler->html();
        // should not be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/cached-if-get-param');
            $this->assertNotEquals($content, $crawler->html());
        }

        $crawler = self::getClient()->request('GET', '/cached-if-get-param?get_param=1');
        $content = $crawler->html();
        // should be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/cached-if-get-param?get_param=1');
            $this->assertEquals($content, $crawler->html());
        }
    }

    public function testRoutesInvalidation(): void
    {
        $crawler = self::getClient()->request('GET', '/cached-if-get-param?get_param=1');
        $content = $crawler->html();

        // should be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/cached-if-get-param?get_param=1');
            $this->assertEquals($content, $cached = $crawler->html());
        }

        // call /always-cached to invalidate
        $crawler = self::getClient()->request('GET', '/always-cached');
        // cache should be cleared
        $crawler = self::getClient()->request('GET', '/cached-if-get-param?get_param=1');
        $this->assertNotEquals($cached, $crawler->html());
    }

    public function testRoutesInvalidationWithCondition(): void
    {
        $crawler = self::getClient()->request('GET', '/cached-if-get-param?get_param=1');
        $content = $crawler->html();
        // should be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/cached-if-get-param?get_param=1');
            $this->assertEquals($content, $cached = $crawler->html());
            break;
        }

        // call /condition/not-invalidate
        $crawler = self::getClient()->request('GET', '/condition/not-invalidate');
        // cache should NOT be cleared
        $crawler = self::getClient()->request('GET', '/cached-if-get-param?get_param=1');
        $this->assertEquals($cached, $crawler->html());

        // call /condition/invalidate to REALLY invalidates
        $crawler = self::getClient()->request('GET', '/condition/invalidate');
        // cache should be cleared
        $crawler = self::getClient()->request('GET', '/cached-if-get-param?get_param=1');
        $this->assertNotEquals($cached, $crawler->html());
    }

    public function testEntityInvalidation(): void
    {
        $em = self::getDoctrine()->getManager();
        $crawler = self::getClient()->request('GET', '/always-cached');
        $cached = $crawler->html();

        // response should be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/always-cached');
            $this->assertEquals($cached, $crawler->html());
        }

        // persist entity should clear cache
        $entity = new Entity();
        $em->persist($entity);
        $crawler = self::getClient()->request('GET', '/always-cached');
        $this->assertNotEquals($cached, $crawler->html());

        $em->flush();
        $crawler = self::getClient()->request('GET', '/always-cached');
        $this->assertNotEquals($cached, $crawler->html());

        // cleaning
        $em->remove($entity);
        $em->flush();
    }

    public function testCustomizedInvalidation(): void
    {
        $crawler = self::getClient()->request('GET', '/get-and-post');
        $cached = $crawler->html();

        // should be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/get-and-post');
            $this->assertEquals($cached, $crawler->html());
        }

        // invalidate with POST
        $crawler = self::getClient()->request('POST', '/get-and-post');
        // should be cached from POST request
        $crawler = self::getClient()->request('GET', '/get-and-post');
        $this->assertNotEquals($cached, $crawler->html());
    }

    public function testCustomizedInvalidationWithCondition(): void
    {
        $crawler = self::getClient()->request('GET', '/condition/CACHED');
        $cached = $crawler->html();

        // should be cached
        for ($x = 0; $x <= self::NB_CHECKS; ++$x) {
            $crawler = self::getClient()->request('GET', '/condition/CACHED');
            $this->assertEquals($cached, $crawler->html());
        }

        // call /always-cached to invalidate /condition/CACHED
        $crawler = self::getClient()->request('GET', '/always-cached');
        // /condition/CACHED should be different
        $crawler = self::getClient()->request('GET', '/condition/CACHED');
        $this->assertNotEquals($cached, $crawler->html());
    }
}
