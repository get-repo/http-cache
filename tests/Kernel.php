<?php

namespace Test;

use GetRepo\HttpCache\GetRepoHttpCacheBundle;
use GetRepo\SqliteDoctrineTest\AbstractKernel;
use Symfony\Bundle\SecurityBundle\SecurityBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use Test\Entity\Entity;
use Test\Entity\User;

class Kernel extends AbstractKernel
{
    /** @return \Symfony\Component\HttpKernel\Bundle\Bundle[] */
    public function registeredBundles(): array
    {
        return [
            new SecurityBundle(),
            new GetRepoHttpCacheBundle(),
        ];
    }

    public function registeredContainerConfiguration(ContainerBuilder $builder, LoaderInterface $loader): void
    {
        $builder->loadFromExtension('security', [
            'providers' => [
                'user_provider' => ['entity' => ['class' => User::class, 'property' => 'name']]
            ],
            'firewalls' => [
                'main' => [
                    'provider' => 'user_provider',
                    'http_basic' => null,
                ],
            ],
        ]);

        $builder->loadFromExtension('getrepo_httpcache', [
            'enabled' => 'not(request.query.has("FORCE_DISABLED"))',
            // override cache factory class to set FilesystemAdapter directory
            'cache_factory' => CacheFactory::class,
            'definitions' => [
                'always_cached' => [
                    'enabled' => 'not(request.query.has("FORCE_DISABLED_ALWAYS_CACHED"))',
                    'cache' => '"KEY_ALWAYS_CACHED"',
                    'invalidations' => [
                        'customized' => ['"CONDITION"'],
                        'orm' => [
                            Entity::class => '"KEY_ALWAYS_CACHED"',
                        ],
                    ],
                ],
                'cached_if_get_param' => [
                    'cache' => [
                        'condition' => 'request.query.all()',
                        'key' => '"CACHED_GET_PARAM"',
                    ],
                    'invalidations' => [
                        'routes' => [
                            'always_cached' => '"CACHED_GET_PARAM"',
                            'condition_name' => [
                                'condition' => 'request.attributes.get("name") == "invalidate"',
                                'key' => '"CACHED_GET_PARAM"',
                            ],
                        ],
                    ],
                ],
                'disabled' => [
                    'enabled' => false,
                    'cache' => '"DISABLED"',
                ],
                'condition_name' => [
                    'cache' => [
                        'condition' => 'request.attributes.get("name") == "CACHED"',
                        'key' => '"CONDITION"',
                    ],
                ],
                'get_and_post' => [
                    'cache' => '"GET_&_POST"',
                    'invalidations' => [
                        'customized' => [
                            ['key' => '"GET_&_POST"', 'condition' => 'request.getMethod() == "POST"'],
                        ],
                    ],
                ],
            ],
        ]);
    }

    protected function getDoctrineORMConfiguration(): array
    {
        return [
            'mappings' => [
                'test' => [
                    'type' => 'attribute',
                    'prefix' => 'Test\Entity',
                    'dir' => __DIR__ . '/Entity',
                ],
            ],
        ];
    }

    public function responseAction(): Response
    {
        return new Response(uniqid());
    }

    protected function configureRoutes(RoutingConfigurator $routes): void
    {
        $controller = $this->responseAction(...);

        $routes->add('not_cached', '/not-cached')->controller($controller);
        $routes->add('disabled', '/disabled')->controller($controller);
        $routes->add('always_cached', '/always-cached')->controller($controller);
        $routes->add('cached_if_get_param', '/cached-if-get-param')->controller($controller);
        $routes->add('condition_name', '/condition/{name}')->controller($controller);
        $routes->add('get_and_post', '/get-and-post')->controller($controller);
    }
}
